#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2018, Alessio Quaresima

# This code is part of a research project on neural network, you are
# free to use it citing me and Giorgio Gosti as original authors.
# it is licensed with MIT license 2018.

import numpy as np
__all__ = [
    'plot_trials',
    'plot_average',
    'plot_similarity_vs_basin'
]

def plot_trials(results):
    assert(isinstance(results,dict))
    import matplotlib.pyplot as plt
    fig, (ax1,ax2) = plt.subplots(2,1)
    for result in results.values():
        ax1.set_title("size of attractor basin")
        ax1.plot(range(len(result["basin_size"].values())),
                sorted([x for x in result["basin_size"].values()],
                        reverse=True), ls="-", marker="o")
        ax2.set_title("cycle length of the loops")
        ax2.plot(range(len(result["loops"])),
                    sorted([len(x) for x in result["loops"].values()],
                        reverse=True), ls="-", marker="o")
    plt.tight_layout()


def plot_average(average):
    import matplotlib.pyplot as plt
    fig, (ax1,ax2,ax3) = plt.subplots(3,1)
    ax1.set_title("average size of attractor basin")
    ax1.errorbar(range(average['max_size']), average['basin_size']['values'],
                    yerr = average['basin_size']['errors'])
    ax2.set_title("average length of loop cycles")
    ax2.errorbar(range(average['max_size']), average['loops_length']['values'],
                    yerr = average['loops_length']['errors'])
    ax3.bar(range(average['max_size']), np.array(average['loops_length']['samples'])
            /float(average['samples_nr']))
    plt.tight_layout()


def plot_similarity_vs_basin(similarity_dict, data):
    import numpy as np
    import matplotlib.pyplot as plt
    def basin(sample):
        bas = data[sample]['graph']['basin_size']
        return {k:bas[k] for k in bas if bas[k] != 0}
    one_list=[]
    for sample in data:
        one_list.extend(zip(basin(sample).values(),
                        similarity_dict[sample].values()))
    one_list = np.array(one_list)
    # import pdb; pdb.set_trace()  # XXX BREAKPOINT

    mean = np.mean(one_list[:,1])
    fig, (ax1) = plt.subplots(1,1)
    for vector in one_list:
        ax1.hist(one_list[:,1], weights=one_list[:,0], color='b')
        # ax1.scatter(one_list[:,1],one_list[:,0], color='r')
        # ax2.plot(one_list[:,1], color='b')
        # ax2.plot([0,one_list.shape[0]], [mean, mean], c='r')
    plt.tight_layout()



# def visualize_results(average):
    # import matplotlib.pyplot as plt
    # fig, (ax1,ax2) = plt.subplots(2,1)
    # assert(isinstance(average,dict))
    # for result in
        # ax1.set_title("size of attractor basin")
        # ax1.plot(range(len(result["basin_size"])),
                # sorted([len(x) for x in result["basin_size"]],
                        # reverse=True), ls="-", marker="o")
        # ax2.set_title("cycle length of the loops")
        # ax2.plot(range(len(result["loops"])),
                    # sorted([len(x) for x in result["loops"]],
                        # reverse=True), ls="-", marker="o")
    # distribution={}
    # for field in ["basin_size", "loops"]:
        # mean = np.mean(np.array([len(x)
                                # for result in results for x in result[field]]))
        # variance = np.var(np.array([len(x)
                                # for result in results for x in result[field]]))
        # distribution[field] = (mean, np.sqrt(variance))

    # plt.tight_layout()
    # plt.show(block=True)
    # #### plot basin size
    # return distribution
