#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2018, Alessio Quaresima

# This code is part of a research project on neural network, you are
# free to use it citing me and Giorgio Gosti as original authors.
# it is licensed with MIT license 2018.

from learningRNN import stateIndex2stateVecSeq as _2vecSeq
from learningRNN import stateIndex2stateVec as _2vec
import numpy as np

def vector_similarity(vec1, vec2, N):
    return np.sum(vec1*vec2)/float(N)

def attractor_basin_similarity(attractor_cycle, basin):
    '''
    '''
    if isinstance(attractor_cycle,list):
        size_array = attractor_cycle.shape[1], basin.shape[0]
        similarity = np.ndarray(size_array)
        size = attractor_cycle.shape[0]
        for i,vector in enumerate(basin):
            for j,attractor in enumerate(attractor_cycle):
                similarity[j,i] = vector_similarity(vector, attractor, size )
        return similarity
    else:
        size_array = 1, basin.shape[0]
        similarity = np.ndarray(size_array)
        size = attractor_cycle.shape[0]
        for i,vector in enumerate(basin):
                similarity[0,i] = vector_similarity(vector,
                                                    attractor_cycle, size )
        return similarity

# def average_similarity(similarity_dictionary):
    # for sample in similarity_dictionary:
        # for key in similarity_dictionary[sample]:
            # similarity_dictionary[sample][key]= \
                # np.mean(similarity_dictionary[sample][key])
    # return similarity_dictionary

def _inverse_similarity_parralelized(sample_inverse, size):
    similarity = {}
    for key in sample_inverse:
        similarity[key] = np.mean(attractor_basin_similarity( _2vec(int(key),size),
                                    _2vecSeq(sample_inverse[key],size)))

    return similarity

def inverse_similarity(data, params):
    '''
    Compute the cosine similarity between the
    '''
    from multiprocessing import Pool
    pool = Pool(processes=7)
    size = int(params['size'])
    similarity ={}
    for sample in data:
        sample_inverse= data[sample]['graph']['inverse']
        similarity[sample] = pool.apply_async(_inverse_similarity_parralelized,
                               (sample_inverse,size,)).get()

    # import pdb; pdb.set_trace()  # XXX BREAKPOINT

    return similarity





## function to produce the average similarity
# def basin(x):
# for x in range(1,10):
     # ...:     z.extend(zip(s[str(x)].values(),basin(str(x)).values()))





