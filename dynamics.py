# -*- coding: utf-8 -*-

import learningRNN as lrnn
import scipy.sparse as sps
import numpy as np
import time

__all__ = [
    "characterize_matrix",
    "generate_activity_matrix",
    "stochastic_generation",
    "learn_matrix"
]

def stochastic_generation(connectivity_matrix,
                          batch_size= 20,
                          explored_fraction= None,
                          time_=False):
    """
    Starting from a fraction of the possible states evolve by the connectivity
    matrix and store only the last explored sites (final_states).
    These states will be inside the cycle or will be the closest
    to the cycle of the previous explored set.

    Eventually evolve the final_states with deterministic dynamics
    and search for cycles.

    Parameters:
    ===========
    connectivity_matrix: np.array(2D) - the matrix of connection with
                                        excitatory and inhibitory stimulus.

    N: int - number of neurons
    batch_size: is the number of states explored in parallel
    explored_fraction: the fraction of neurons to investigate, it is equivalent
            to the maximum of steps realized

    1. Start from random state and walk down the hill,
    2. After a certain time (Binomial? Exponential? Power law?) jump towards
    another randomly chosen state.
    3. After 2^N steps you will have explored the whole set of states
    """
    import collections
    def random_state_generator(N, n):
        while True:
            if N< 62:
                ran = np.random.randint(0,np.power(2,N),(1,n)).tolist()[0]
            else:
                ran = np.random.randint(0,np.power(2,62),(1,n)).tolist()[0]
            yield lrnn.stateIndex2stateVecSeq(ran, N, 0)

    N = connectivity_matrix.shape[1]
    random_states = random_state_generator(N, batch_size)
    counter = collections.Counter
    states_buffer = []
    walked=0
    if explored_fraction is None:
        WALKED_MAX = 2**(N/10)
    else:
        WALKED_MAX = 2**(int(N/explored_fraction))
    while (walked < WALKED_MAX):
        states = next(random_states)
        # import pdb; pdb.set_trace()  # XXX BREAKPOINT
        n = 0
        while (n < np.log(WALKED_MAX)):
            states = lrnn.transPy(states,connectivity_matrix, N, 0, 0)
            states_buffer.extend(lrnn.stateVec2stateIndexSeq(states,N,0))
            n +=1
            # states_buffer.extend(lrnn.stateVec2stateIndexSeq(states,N,0))
            walked +=1

    # return states_buffer, counter(states_buffer),
    states = counter(states_buffer).keys()
    return states




def generate_activity_matrix(connectivity_matrix, states=None, time_=False):
    """
    Generate the activity matrix within deterministic framework:
    Creates a vector of all initial states (2^N) and their successive states
    and associates each other producing a sparse matrix

    Parameters:
    ===========
    connectivity_matrix: np.array(2D) - the matrix of connection with
                                        excitatory and inhibitory stimulus.

    N: int - number of neurons
    time_: boolean - print the timestamp o execution time.

    """
    typ = 0
    thr = 0
    N = connectivity_matrix.shape[1]
    def all_states(N,typ):
        return lrnn.stateIndex2stateVecSeq(range(np.power(2,N)), N, typ)
    mytime = time.time()

    if states is None:
        initial_states = all_states(N,typ)
    else:
        if not isinstance(states, list):
            raise ValueError("states is expected to be a list")
        initial_states = lrnn.stateIndex2stateVecSeq(states, N, typ)
    final_states = lrnn.transPy(initial_states,
                                   connectivity_matrix, N, typ, thr)

    ## it is now possible to generate all the recurrent transitions, this can be usefull
    # transition_list_2 = lrnn.transPy(final_states,objective, N, typ, thr)
    # transition_list_3 = lrnn.transPy(transition_list_2,objective, N, typ, thr)

    ## generate the sparse matrix
    ### se un istanza di lista allora sto usando il batch,
    ### altrimenti era un istanza sdi numero
    rows = len(initial_states)
    cols = len(final_states)

    if np.all(initial_states == final_states) is True:
        sps_acc = sps.lil_matrix((rows, cols), dtype=np.bool) # empty matrix
        for pre,post in zip(initial_states, final_states): # add 100 sets of 100 1's
            pre = lrnn.stateVec2stateIndex(pre, N, typ)
            post = lrnn.stateVec2stateIndex(post, N, typ)
            sps_acc[pre,post]=True
    else:
        import networkx as nx
        G = nx.DiGraph()
        for pre,post in zip(initial_states, final_states): # add 100 sets of 100 1's
            pre = lrnn.stateVec2stateIndex(pre, N, typ)
            post = lrnn.stateVec2stateIndex(post, N, typ)
            G.add_edge(pre,post)

        sps_acc = nx.convert_matrix.to_scipy_sparse_matrix(G, dtype=np.bool)

    mytime = time.time() - mytime
    if time_ :
        print("execution time: {}".format(mytime))
    return sps_acc

def characterize_matrix(sparse_matrix):
    def single_matrix_characterize(matrix):
        from networkx.readwrite import json_graph
        G = nx.from_scipy_sparse_matrix(matrix,create_using=nx.DiGraph())
        ## 1. find self loop
        # self_loop = [matrix[x,x] for x in range(np.power(2,
                                            # matrix.shape))]
        # self_loop=None
        ## 2. find basin attractors
        basin_size = [len(basin) for basin in list(nx.weakly_connected_components(G))]
        ## 3. find loops
        loops = list(nx.cycles.simple_cycles(G))
        def list_to_dict(lista):
            dct={}
            for num, elem in enumerate(lista):
                dct[num]=elem
            return dct

        graph = {"direct":{},"inverse":{}, "basin_size":{}}
        for node in G.nodes.keys():
            graph["direct"][node] = G[node].keys()
            size = len(G.in_edges(node))
            if size>0:
                graph["inverse"][node] = [edge[0] for edge
                                        in list(G.in_edges(node))]
                graph["basin_size"][node] = size
            else:
                graph["basin_size"][node] = 0
        return {"basin_size":list_to_dict(basin_size),
                "loops":list_to_dict(loops),
                "graph" : graph
                }

    import networkx as nx
    if isinstance(sparse_matrix, list):
        results = {enum:single_matrix_characterize(matrix) for enum,matrix
                   in enumerate(sparse_matrix)}
    else:
        results = single_matrix_characterize(sparse_matrix)

    return results



def learn_matrix(connectivity_matrix, N, seed=None,
                        train_set_size=1,
                        initial_state=None):

    T = 1000 # Numero step per convergenza
    typ = 0
    thr = 0
    alpha = 10.
    nP = {"N":N, "typ":typ, "thr": thr}

    # Fare training della matrice stimata
    ###############################################################################

    # in questo modo ho una lista da cui ricavo training set e validation set
    # seqs = list(np.hstack((trajectory,  cycle[0])))

    # creo molte traiettorie con seed differenti, a partire
    # da initial state differenti, poi le impilo in una lista e le passo
    # alla funzione che mi ritorna il training set

    # create initial state
    if seed is not None:
        np.random.seed(seed)
    seeds = np.random.choice((2**N) , size=train_set_size, replace=False)

    seqs = []
    for i,sm in enumerate(seeds):
        cicli1,path1 = lrnn.getTrajPy(sm, connectivity_matrix,N,0,0,100000)
        seq1 = list(path1)+[cicli1[0]]
        #print len(seq1)
        seqs.append(seq1)

    X_train, Y_train = lrnn.makeTrainXYfromSeqs(seqs, nP, isIndex= True)

    # gdStrat = gradientdescend strategy
    trained_matrix, deltas, fullDeltas, exTime, convStep =\
        lrnn.runGradientDescent(X_train, Y_train, alpha0= 0.0, alphaHat=alpha,
                            batchFr = 1.0, passi=T, runSeed = 3098, gdStrat="GD", k=1, netPars=nP,
                            showGradStep= None, xi= 0.000, mexpon = -1.5)

    import matplotlib.pyplot as plt
    plt.pcolor(trained_matrix)
    fig, (ax1,ax2)= plt.subplots(2)
    ax1.pcolor(trained_matrix)
    ax2.pcolor(objective)
    plt.show(block=True)



