#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2018, Alessio Quaresima

# This code is part of a research project on neural network, you are
# free to use it citing me and Giorgio Gosti as original authors.
# it is licensed with MIT license 2018.

import numpy as np
__all__=[
"get_average"
]

def get_average(results):
    average = {
                "loops_length": average_loops_length(results),
                "loops_number": average_number_of_loops(results),
                "basin_size":   average_basin_size(results),
                'max_size'  :   max_size(results),
                'samples_nr':   len(results)
            }
    return average

def position_wise_sum(elements):
    '''
    This function operates the average among the samples, it takes care the
    attractors for each network are ordered by their size.
    It is usefull to distinguish the giant attractors by the others, it
    should preserve the basin_size distributions of the connectivity matrix
    with parameters N, epsilon, rho.

    taking care of
    attractor relative ordering in terms of basin size.

    '''
    sums = {}
    sums_square = {}
    counter ={}
    for elem in elements:
        position = 0
        while True:
            try:
                sums[position] = elem [position] if not position in sums \
                    else elem[position] + sums[position]
                sums_square[position] = elem[position]**2 if not position in sums \
                    else elem[position]**2 + sums[position]
                counter[position] = 1 if not position in counter \
                    else 1 + counter[position]
                position += 1
            except:
                break
    mean = list(np.array(sums.values())/np.array(counter.values(),dtype=float))
    var =np.array(sums_square.values())/np.array(counter.values(),dtype=float) - mean
    return { 'values':mean, 'errors':list(np.sqrt(var)), 'samples':counter.values()}

def average_loops_length(results):
    '''
    Characterize the networks' loops with averages on batch runs
    '''
    return position_wise_sum([[len(loop) for loop in result["loops"].values()] for result in results.values()])

def max_size(results):
    return np.max([len(result['loops'].values()) for result in results.values()])


def average_basin_size(results):

    return position_wise_sum([[basin for basin in result["basin_size"].values()] for result in results.values()])



def average_number_of_loops(results):

    return np.mean([len(result["loops"].values()) for result in results.values()]),\
        np.var([len(result["loops"].values()) for result in results.values()])

