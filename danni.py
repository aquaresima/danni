#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2018, Alessio Quaresima

# This code is part of a research project on neural network, you are
# free to use it citing me and Giorgio Gosti as original authors.
# it is licensed with MIT license 2018.

from dynamics import stochastic_generation
from dynamics import generate_activity_matrix
from dynamics import characterize_matrix
from connectivity_matrix import get_connectivity_matrix
import learningRNN as lrnn
from utils import timeit
from utils import save_to_file

from multiprocessing import Pool
import numpy as np
import functools
import logging

####
# Utility functions to estimate time
####

@timeit
def deterministic_run(N, batch_size,
                      connectivity_matrix_s=None,
                      seed=None, **kwargs):
    """
    Run deterministic exploration of all 2^N states
    """
    if seed is not None:
        np.random.seed(seed)
    seeds = np.random.randint(1, 10000, (1, batch_size)).tolist()[0]
    if connectivity_matrix_s is None:
        connectivity_matrix_s = [get_connectivity_matrix(N, seed, **kwargs)
                                 for seed in seeds]

## For each connectivity_matrix produce the dynamic matrix
## which is the non invertible function on 2^N space
## This matrix is highly sparse, only 2^N elements are different
## from zero and equal to one.
    if kwargs.get("multiprocessing") is True:
        ### HARDCODED NUMBER OF PROCESSORS FOR THE CLUSTER I AM USING
        ### @TODO Pass it with args.nprocs
        pool = Pool(processes=12)
        dynamic_matrix_s = pool.map(
            generate_activity_matrix, connectivity_matrix_s)
    else:
        dynamic_matrix_s = map(
            generate_activity_matrix, connectivity_matrix_s)
    return dynamic_matrix_s, connectivity_matrix_s


@timeit
def stochastic_run(N, batch_size,
                   connectivity_matrix_s=None,
                   seed=None, **kwargs):
    """
    Process and extract the info on Attractors with statistical approach

    Parameters:
    ===========
    N: is the number of neurons in the network
    batch_size: is the number of states explored in parallel
    seed: set the seed of numpy random
    connectivity_matrix_s: set of connectivity matrix for evolving the network

    Returns:
    ========
    states: the whole set of states explored during the stochastic running
    matrix_s: the matrix resulting by the the deterministic transition list
            of states
    """
    # init the batch processing
    if seed is not None:
        np.random.seed(seed)
    seeds = np.random.randint(1, 10000, (1, batch_size)).tolist()[0]
    if connectivity_matrix_s is None:
        connectivity_matrix_s = [get_connectivity_matrix(N, seed, **kwargs)
                                 for seed in seeds]

    pool = Pool(processes=6)
    function = functools.partial(stochastic_generation,
                                 batch_size=batch_size)
    states_s = pool.map(function, connectivity_matrix_s)
    matrix_s = []
    print("info: N is {} ".format(N))
    print("(states list analyzed, matrix size)")
    for states, connectivity_matrix in zip(states_s, connectivity_matrix_s):
        matrix_s.append(generate_activity_matrix(connectivity_matrix,
                                                 states, time_=False))
        print("({}, {})".format(len(states), matrix_s[-1].shape))
    return states_s, matrix_s, connectivity_matrix_s


def main_process_data(args):
    from utils import load_data_from_json
    from corr_analysis import inverse_similarity
    data = load_data_from_json(args.data_file)
    try:
        params = data['params']
    except KeyError:
        params = data['details']
    try:
        data = data['data']
    except KeyError:
        data = data['results']
    similarity = inverse_similarity(data, params)
    from attractor_analysis import get_average
    average = get_average(data)
    save_to_file(args, average, folder="results")
    logging.info("data analysis accomplished, preparing plot")
    if args.plot:
        from os.path import join
        from visualize import plot_trials, plot_average
        from visualize import plot_similarity_vs_basin
        import matplotlib.pyplot as plt

        save_string ='rho{}_eps{}_size{}_smpls{}'.format(
            params['rho'],params['epsilon'],
            params['size'],params['batch_size'])
        plot_trials(data)
        plt.savefig(join("plots",save_string+"_trials.pdf"))
        # plot_average(average)
        # plt.savefig(join("plots",save_string+"_avg.pdf"))
        plot_similarity_vs_basin(similarity, data)
        plt.savefig(join("plots",save_string+"_similarity.pdf"))
    logging.info("plot saved to file")
        # plt.show(block=True)
    # save_to_file(args, data)



def main_generate_data(args):
    if args.mode == "deterministic":
        logging.debug("run into deterministic mode")
        dynamic_matrix_s, connectivity_s = deterministic_run(args.size,
                                                args.batch_size,
                                                rho=args.rho,
                                                epsilon=args.epsilon,
                                                seed=args.seed,
                                multiprocessing=args.multiprocessing)
        logging.debug("dynamic matrix computed")
        data = characterize_matrix(dynamic_matrix_s)
        main_generate_data.dynamic_matrix_s = dynamic_matrix_s



    if args.mode == "stochastic":
        states, matrix_s, connectivity_s = stochastic_run(args.size,
                                            args.batch_size,
                                            rho=args.rho,
                                            epsilon=args.epsilon,
                                            seed=args.seed)
        data = characterize_matrix(matrix_s)
        main_generate_data.states = states
        main_generate_data.matrix_s = matrix_s

    main_generate_data.connectivity_s = connectivity_s
    main_generate_data.data = data
    save_to_file(args, data, folder="data")
    from attractor_analysis import get_average
    average = get_average(data)
    save_to_file(args, average, folder="results")


def main_timeit(args):
    log_time = {}
    log_data = {}
    for N in np.logspace(1.1, 1.8, 15):
        N = int(N)
        states, matrix_s = stochastic_run(N, args.batch_size,
                                            log_time=log_time,
                                            log_name=str(N))
        data = characterize_matrix(matrix_s)

    import matplotlib.pyplot as plt
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
    for x in sorted(log_data.keys()):
        ax1.scatter(x, log_data[x]["loops_number"][0], c="b", marker=".")
        ax2.scatter(x, log_data[x]["loops_length"][0], c="b", marker=".")
    ax1.set_title("loops number")
    ax2.set_title("loops length")
    ax2.set_xlabel("number of neurons")

        # data, distribution, dynamic_matrix_s = stochastic_run()
def main_learn(args):
    from learningRNN import learn_matrix
    N = args.size
    num_genr, objective = lrnn.generateSmallWorldBase(
        N, 3, 0.3, rseed=2219)
    learn_matrix(objective, N, args.train_set_size)

    # import matplotlib.pyplot as plt
    # fig, (ax1) = plt.subfigs(1,1)
    # # for states, result in zip(states_s, data):
    # ax1.plot([len(states) for states in states_s])
    # z =[ len(result["graph"].nodes()) for result in data]



