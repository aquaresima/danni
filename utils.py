#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2018, Alessio Quaresima

# This code is part of a research project on neural network, you are
# free to use it citing me and Giorgio Gosti as original authors.
# it is licensed with MIT license 2018.

import argparse
from json import JSONEncoder
import numpy as np
import logging

class MyEncoder(JSONEncoder):
        def default(self, o):
            if isinstance(o, np.int32):
                return int(o)
            if isinstance(o, bool) or isinstance(o, np.bool_):
                return np.bool(o)
            else:
                return JSONEncoder.default(self,o)


def parse_arguments():
    """
    Parser import arguments from command line
    """
    parser = argparse.ArgumentParser()
    settings = parser.add_argument_group("Settings")
    settings.add_argument('--plot',  default=False, action ='store_true')
    settings.add_argument('--log',  default="WARNING", type =str)
    settings.add_argument('--logfile',  default=None, type =str)
    settings.add_argument('--data_folder',  default="data", type =str)
    settings.add_argument("--multiprocessing", default=False, action='store_true')
    settings.add_argument('--config_file',type=str, default=None,
                             help ='File with configuration parameters')
    settings.add_argument('--data_file',type=str, default=None,
                             help ='JSON File with data')


    execution = parser.add_mutually_exclusive_group()
    execution.add_argument('--generate_data', action='store_true', default=False)
    execution.add_argument('--process_data', action='store_true', default=False)
    execution.add_argument('--learn', action='store_true', default=False)
    execution.add_argument('--timeit', action='store_true', default=False)
    execution.add_argument('--single_trial', action='store_true', default=False)

    parameters = parser.add_argument_group("Parameters")
    parameters.add_argument('--batch_size', dest="batch_size", default = 0,
                        type=int, help='Size of parallelized stack')
    parameters.add_argument('--mode', dest="mode", default=None, type=str)
    parameters.add_argument('--epsilon', dest="epsilon", default=0.5,
                        type=float, help='asimmetry parameter of connectivity'
                        'matrix')
    parameters.add_argument('--rho', dest="rho", default=0.5,
                        type=float, help='sparsity parameter of connectivity'
                        'matrix')
    parameters.add_argument('--seed', dest="seed", type=int, default=23,
                        help='Seed for Random Generator')
    parameters.add_argument('--train_set_size', dest="train_set_size",
                        type=int, help='Size of train set"\
                        "for connectivity matrix estimation')
    parameters.add_argument('--size', type=int,
                        help='Number of neurons in the Network')
    return parser.parse_args()


def timeit(method):
    """
    This is a standard decorator to log execution time
    """
    import time

    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()

        name = kw.get('log_name', method.__name__.upper())
        elapsed_time = (te - ts) * 1000
        if 'log_time' in kw:
            kw['log_time'][name] = elapsed_time
            # print("{}: {} ms".format(name, int(te - ts) * 1000))
        # else:
        logging.info("{} elapsed time: {}".format(name,elapsed_time))
        return result

    return timed

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

def args_from_settings(settings_file):
    import json
    # import pdb; pdb.set_trace()  # XXX BREAKPOINT

    with open(settings_file,"r") as fp:
        params = json.load(fp)
    return params

@timeit
def load_data_from_json(file_):
    import json
    with open(file_, "r") as fp:
        results = json.load(fp)
    return results

@timeit
def save_to_file(params, data, folder):
    from os.path import join
    from json import dump
    from json import encoder

    encoder.FLOAT_REPR = lambda o: format(o, '.2f')
    save_dict={}

    save_dict['data']=data
    args = AttrDict(params)
    save_string ='rho{}_eps{}_size{}_smpls{}'.format(
                    args.rho,args.epsilon,args.size,args.batch_size)
    save_details = {'rho':args.rho,
                    'epsilon':args.epsilon,
                    'size':args.size,
                    'batch_size':args.batch_size,
                    'seed':args.seed,
                    'mode':args.mode,
                    }
    save_dict['params']=save_details
    save_path = join(folder,save_string+".json")
    logging.info("Writing file: {}".format(save_path))
    with open(save_path,'w') as fp:
        dump(save_dict, fp, cls = MyEncoder)
