from danni import main_generate_data, main_process_data
from danni import main_timeit, main_learn
from utils import parse_arguments
from utils import args_from_settings, AttrDict
import logging, datetime

if __name__ == "__main__":
    args = parse_arguments()
    loglevel = args.log
    loglevel = getattr(logging, loglevel.upper())
    logging.basicConfig(level=loglevel, filename=args.logfile)
    logging.info("==================")
    logging.info("Running danni")
    logging.info(datetime.datetime.now())

    if args.generate_data:
        logging.info("Generate activity matix and characterize attractors")
        if args.config_file is not None:
            config_file = args.config_file
            args = vars(args)
            logging.info("Loading parameters by {} ".format(config_file))
            args.update(args_from_settings(config_file))
            args = AttrDict(args)
            logging.info('Log for simulation \n \trho: {} \n\teps: {}\n\tsize'
                        ' {}\n\tsamples {} \n\tmultiprocessing: {}'.format(
                            args.rho,args.epsilon,
                            args.size,args.batch_size,
                            args.multiprocessing))
        else:
            logging.warnings("Parameters by command line only")
            logging.info('Log for simulation \n \trho: {} \n\teps: {}\n\tsize'
                        ' {}\n\tsamples {} \n\tmultiprocessing: {}'.format(
                            args.rho,args.epsilon,
                            args.size,args.batch_size,
                            args.multiprocessing))
        main_generate_data(args)

    if args.process_data:
        logging.info("Perform data analysis from {}".format(args.data_file))
        import os
        assert(args.data_file is not None)
        main_process_data(args)

    if args.timeit:
        main_timeit(args)

    if args.learn:
        main_learn(args)

    logging.info("==================")
    logging.info("  danni is over")

