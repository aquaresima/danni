
I prima cosa ho implementato una
semplice interfaccia per l'uso della
libreria learninRNN.py

L'interfaccia (run_learninRNN) crea una
matrice casuale delle interazioni, a
seguire allena con un algoritmo di
back_propagation una nuova matrice e
cerca di riprodurre la stessa dinamica
per un set di stati iniziali.
![Matrice di allenamento e matrice allenata](trained_matrix.png)

https://www5.unitn.it/Apply/en/Web/Home/dott
e fa una sessione di
ottimizzazione della stessa
